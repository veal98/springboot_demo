package com.smallbeef.springboot_demo.dao;


import com.smallbeef.springboot_demo.bean.Department;
import com.smallbeef.springboot_demo.bean.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * 员工 Dao
 */
@Repository
public class EmployeeDao {

    // 模拟数据库中的数据
    private static Map<Integer, Employee> employees = null;
    // 员工有所属的部门
    @Autowired
    private DepartmentDao departmentDao;
    static{
        employees = new HashMap<>(); // 创建一个员工表
        employees.put(1001, new Employee(1001, "AA", "123456@qq.com", 1, new Department(101,"教学部")));
        employees.put(1002, new Employee(1002, "BB", "1546556@qq.com", 0, new Department(102, "市场部")));
        employees.put(1003, new Employee(1003, "CC", "765543@qq.com", 1, new Department(103, "教研部")));
        employees.put(1004, new Employee(1004, "DD", "34654234@qq.com", 0, new Department(104, "运营部")));
        employees.put(1005, new Employee(1005, "EE", "72423423456@qq.com", 1, new Department(105, "后勤部")));
    }

    // 增加一个员工（主键自增）
    private static Integer initId = 1006;
    public void save(Employee employee){
        if(employee.getId() == null) // 设置Id
            employee.setId(initId ++);
        employee.setDepartment(departmentDao.getDepartmentById(employee.getDepartment().getId())); // 设置部门
        employees.put(employee.getId(),employee); // 放入 Map (数据库)
    }

    // 查询全部员工信息
    public Collection<Employee> getAll(){
        return employees.values();
    }

    // 根据 id 查询员工信息
    public Employee getEmployeeById(Integer id){
        System.out.println("EmplyeeDao 根据 id 查询员工信息");
        return employees.get(id);
    }

    // 根据 id 删除员工
    public void delete(Integer id){
        System.out.println("EmplyeeDao 根据 id 删除员工");
        employees.remove(id);
    }

}
