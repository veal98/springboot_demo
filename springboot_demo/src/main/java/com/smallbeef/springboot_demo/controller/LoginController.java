package com.smallbeef.springboot_demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {

    @RequestMapping("/user/login")
    public String login(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            Model model, HttpSession session){

        // 简单的登录验证
        if(!StringUtils.isEmpty(username) && "12345".equals(password)){
            session.setAttribute("loginUser", username);
            return "redirect:/main.html";
        }
        else{
            // 显示登录失败信息
            model.addAttribute("msg","用户名或者密码错误！");
            return "index";
        }
    }
}
