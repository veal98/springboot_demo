package com.smallbeef.springboot_demo.config;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginHandlerIntercepter implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        // 登录成功后 ，应该有用户的 session
        Object loginUser = request.getSession().getAttribute("loginUser");

        if(loginUser == null){ // 未登录
            request.setAttribute("msg", "没有权限，请先登录");
            request.getRequestDispatcher("/index.html").forward(request,response);
            return false; // 表示拦截
        }
        else
            return true; // 表示放行
    }
}
